<?xml version="1.0" ?><!DOCTYPE TS><TS language="zh_TW" sourcelanguage="en" version="2.0">
<context>
    <name>AddAddressDialog</name>
    <message>
        <location filename="../bitmessageqt/addaddressdialog.py" line="62"/>
        <source>Add new entry</source>
        <translation>添加新條目</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/addaddressdialog.py" line="63"/>
        <source>Label</source>
        <translation>標籤</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/addaddressdialog.py" line="64"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
</context>
<context>
    <name>EmailGatewayDialog</name>
    <message>
        <location filename="../bitmessageqt/emailgateway.py" line="67"/>
        <source>Email gateway</source>
        <translation>電子郵件</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/emailgateway.py" line="68"/>
        <source>Register on email gateway</source>
        <translation>註冊電子郵件</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/emailgateway.py" line="69"/>
        <source>Account status at email gateway</source>
        <translation>電子郵件帳戶狀態</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/emailgateway.py" line="70"/>
        <source>Change account settings at email gateway</source>
        <translation>更改電子郵件帳戶設置</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/emailgateway.py" line="71"/>
        <source>Unregister from email gateway</source>
        <translation>取消電子郵件註冊</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/emailgateway.py" line="72"/>
        <source>Email gateway allows you to communicate with email users. Currently, only the Mailchuck email gateway (@mailchuck.com) is available.</source>
        <translation>電子郵件允許您與電子郵件用戶通信。目前只有 Mailchuck 電子郵件（@mailchuck.com）可用。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/emailgateway.py" line="73"/>
        <source>Desired email address (including @mailchuck.com):</source>
        <translation>所需的電子郵件地址（包括 @mailchuck.com）：</translation>
    </message>
</context>
<context>
    <name>EmailGatewayRegistrationDialog</name>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2127"/>
        <source>Registration failed:</source>
        <translation>註冊失敗：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2127"/>
        <source>The requested email address is not available, please try a new one. Fill out the new desired email address (including @mailchuck.com) below:</source>
        <translation>要求的電子郵件地址不詳，請嘗試一個新的。填寫新的所需電子郵件地址（包括 @mailchuck.com）如下：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/emailgateway.py" line="102"/>
        <source>Email gateway registration</source>
        <translation>電子郵件註冊</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/emailgateway.py" line="103"/>
        <source>Email gateway allows you to communicate with email users. Currently, only the Mailchuck email gateway (@mailchuck.com) is available.
Please type the desired email address (including @mailchuck.com) below:</source>
        <translation>電子郵件允許您與電子郵件用戶通信。目前，只有 Mailchuck 電子郵件（@mailchuck.com）可用。請鍵入所需的電子郵件地址（包括 @mailchuck.com）如下：</translation>
    </message>
</context>
<context>
    <name>Mailchuck</name>
    <message>
        <location filename="../bitmessageqt/account.py" line="241"/>
        <source># You can use this to configure your email gateway account
# Uncomment the setting you want to use
# Here are the options:
#
# pgp: server
# The email gateway will create and maintain PGP keys for you and sign, verify,
# encrypt and decrypt on your behalf. When you want to use PGP but are lazy,
# use this. Requires subscription.
#
# pgp: local
# The email gateway will not conduct PGP operations on your behalf. You can
# either not use PGP at all, or use it locally.
#
# attachments: yes
# Incoming attachments in the email will be uploaded to MEGA.nz, and you can
# download them from there by following the link. Requires a subscription.
#
# attachments: no
# Attachments will be ignored.
#
# archive: yes
# Your incoming emails will be archived on the server. Use this if you need
# help with debugging problems or you need a third party proof of emails. This
# however means that the operator of the service will be able to read your
# emails even after they have been delivered to you.
#
# archive: no
# Incoming emails will be deleted from the server as soon as they are relayed
# to you.
#
# masterpubkey_btc: BIP44 xpub key or electrum v1 public seed
# offset_btc: integer (defaults to 0)
# feeamount: number with up to 8 decimal places
# feecurrency: BTC, XBT, USD, EUR or GBP
# Use these if you want to charge people who send you emails. If this is on and
# an unknown person sends you an email, they will be requested to pay the fee
# specified. As this scheme uses deterministic public keys, you will receive
# the money directly. To turn it off again, set "feeamount" to 0. Requires
# subscription.
</source>
        <translation>#您可以用它來配置你的電子郵件帳戶
#取消您要使用的設定
#這裡的選項：
#
# pgp: server
#電子郵件將創建和維護PGP密鑰，為您簽名和驗證，
#代表加密和解密。當你想使用PGP，但懶惰，
#用這個。需要訂閲。
#
# pgp: local
#電子郵件不會代你進行PGP操作。您可以
#選擇或者不使用PGP, 或在本地使用它。
#
# attachement: yes
#傳入附件的電子郵件將會被上傳到MEGA.nz，您可以從
# 按照那裡連結下載。需要訂閲。
#
# attachement: no
#附件將被忽略。
#
# archive: yes
#您收到的郵件將在服務器上存檔。如果您有需要請使用
#幫助調試問題，或者您需要第三方電子郵件的證明。這
#然而，意味著服務的操作運將能夠讀取您的
#電子郵件即使電子郵件已經傳送給你。
#
# archive: no
# 已傳入中繼的電子郵件將從服務器被刪除。
#
# masterpubkey_btc：BIP44 XPUB鍵或琥珀金V1公共種子
#offset_btc：整數（默認為0）
#feeamount：8位小數
#feecurrency號：BTC，XBT，美元，歐元或英鎊
#當你想要向發送電子郵件給你的人收費的時候使用。如果這是在和
#一個不明身份的人向您發送一封電子郵件，他們將被要求支付規定的費用
#。由於這個方案使用確定性的公共密鑰，你會直接接收
#錢。要再次將其關閉，設置“feeamount”0
#需要訂閲。</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="183"/>
        <source>Reply to sender</source>
        <translation>回覆發件人</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="185"/>
        <source>Reply to channel</source>
        <translation>回覆頻道</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="187"/>
        <source>Add sender to your Address Book</source>
        <translation>將發送者添加到您的通訊簿</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="191"/>
        <source>Add sender to your Blacklist</source>
        <translation>將發件人添加到您的黑名單</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="377"/>
        <source>Move to Trash</source>
        <translation>移入垃圾桶</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="198"/>
        <source>Undelete</source>
        <translation>取消刪除</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="201"/>
        <source>View HTML code as formatted text</source>
        <translation>查看HTML</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="205"/>
        <source>Save message as...</source>
        <translation>將消息保存為...</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="209"/>
        <source>Mark Unread</source>
        <translation>標記為未讀</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="349"/>
        <source>New</source>
        <translation>新建</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/blacklist.py" line="121"/>
        <source>Enable</source>
        <translation>啟用</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/blacklist.py" line="124"/>
        <source>Disable</source>
        <translation>禁用</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/blacklist.py" line="127"/>
        <source>Set avatar...</source>
        <translation>設置頭像...</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/blacklist.py" line="117"/>
        <source>Copy address to clipboard</source>
        <translation>將地址複製到剪貼板</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="296"/>
        <source>Special address behavior...</source>
        <translation>特別的地址行為...</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="257"/>
        <source>Email gateway</source>
        <translation>電子郵件</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/blacklist.py" line="114"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="312"/>
        <source>Send message to this address</source>
        <translation>發送消息到這個地址</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="320"/>
        <source>Subscribe to this address</source>
        <translation>訂閲這個地址</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="332"/>
        <source>Add New Address</source>
        <translation>創建新地址</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="380"/>
        <source>Copy destination address to clipboard</source>
        <translation>複製目標地址到剪貼板</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="384"/>
        <source>Force send</source>
        <translation>強制發送</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="598"/>
        <source>One of your addresses, %1, is an old version 1 address. Version 1 addresses are no longer supported. May we delete it now?</source>
        <translation>您的其中一個地址, %1,是一個過時的版本1地址. 版本1地址已經不再支援。 請問想要刪除嗎?</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="991"/>
        <source>Waiting for their encryption key. Will request it again soon.</source>
        <translation>正在等待他們的加密密鑰，我們會在稍後再次請求。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="990"/>
        <source>Encryption key request queued.</source>
        <translation>加密密鑰請求已經列入隊列。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="997"/>
        <source>Queued.</source>
        <translation>已經添加到隊列。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1000"/>
        <source>Message sent. Waiting for acknowledgement. Sent at %1</source>
        <translation>消息已經發送. 正在等待回執. 發送於 %1</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1003"/>
        <source>Message sent. Sent at %1</source>
        <translation>消息已經發送. 發送於 %1</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1002"/>
        <source>Need to do work to send message. Work is queued.</source>
        <translation>需要做些工作來傳送消息，工作已經隊列</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1009"/>
        <source>Acknowledgement of the message received %1</source>
        <translation>消息的回執已經收到於 %1</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1988"/>
        <source>Broadcast queued.</source>
        <translation>廣播已經添加到隊列中。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1018"/>
        <source>Broadcast on %1</source>
        <translation>已經廣播於 %1</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1021"/>
        <source>Problem: The work demanded by the recipient is more difficult than you are willing to do. %1</source>
        <translation>錯誤： 收件人要求的做工量大於我們的最大接受做工量。 %1</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1024"/>
        <source>Problem: The recipient's encryption key is no good. Could not encrypt message. %1</source>
        <translation>錯誤： 收件人的加密密鑰是無效的。不能加密消息。 %1</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1027"/>
        <source>Forced difficulty override. Send should start soon.</source>
        <translation>已經忽略最大做工量限制。發送很快就會開始。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1030"/>
        <source>Unknown status: %1 %2</source>
        <translation>未知狀態： %1 %2</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1534"/>
        <source>Not Connected</source>
        <translation>未連接</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1159"/>
        <source>Show Bitmessage</source>
        <translation>顯示比特信</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="688"/>
        <source>Send</source>
        <translation>發送</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1182"/>
        <source>Subscribe</source>
        <translation>訂閲</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1188"/>
        <source>Channel</source>
        <translation>頻道</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="734"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1405"/>
        <source>You may manage your keys by editing the keys.dat file stored in the same directory as this program. It is important that you back up this file.</source>
        <translation>您可以通過編輯和程序儲存在同一個目錄的 keys.dat 來編輯密鑰。備份這個文件十分重要。 </translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1409"/>
        <source>You may manage your keys by editing the keys.dat file stored in
 %1
It is important that you back up this file.</source>
        <translation>您可以通過編輯儲存在 %1 的 keys.dat 來編輯密鑰。備份這個文件十分重要。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1416"/>
        <source>Open keys.dat?</source>
        <translation>打開 keys.dat ？ </translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1413"/>
        <source>You may manage your keys by editing the keys.dat file stored in the same directory as this program. It is important that you back up this file. Would you like to open the file now? (Be sure to close Bitmessage before making any changes.)</source>
        <translation>您可以通過編輯和程序儲存在同一個目錄的 keys.dat 來編輯密鑰。備份這個文件十分重要。您現在想打開這個文件麼？（請在進行任何修改前關閉比特信）</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1416"/>
        <source>You may manage your keys by editing the keys.dat file stored in
 %1
It is important that you back up this file. Would you like to open the file now? (Be sure to close Bitmessage before making any changes.)</source>
        <translation>您可以通過編輯儲存在 %1 的 keys.dat 來編輯密鑰。備份這個文件十分重要。您現在想打開這個文件麼？（請在進行任何修改前關閉比特信）</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1423"/>
        <source>Delete trash?</source>
        <translation>清空垃圾桶？</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1423"/>
        <source>Are you sure you want to delete all trashed messages?</source>
        <translation>您確定要刪除垃圾桶裡面全部的消息麼？</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1443"/>
        <source>bad passphrase</source>
        <translation>錯誤的密鑰</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1443"/>
        <source>You must type your passphrase. If you don't have one then this is not the form for you.</source>
        <translation>您必須輸入您的密鑰。如果您沒有的話，這個表單不適用於您。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1456"/>
        <source>Bad address version number</source>
        <translation>地址的版本號無效</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1452"/>
        <source>Your address version number must be a number: either 3 or 4.</source>
        <translation>您的地址的版本號必須是一個數字： 3 或 4.</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1456"/>
        <source>Your address version number must be either 3 or 4.</source>
        <translation>您的地址的版本號必須是 3 或 4.</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1608"/>
        <source>Chan name needed</source>
        <translation>需要填入頻道名稱</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1608"/>
        <source>You didn't enter a chan name.</source>
        <translation>您並沒有輸入頻道名稱</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1628"/>
        <source>Address already present</source>
        <translation>地址已經存在</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1628"/>
        <source>Could not add chan because it appears to already be one of your identities.</source>
        <translation>無法新增頻道，因為地址已經是您的身份之一。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1632"/>
        <source>Success</source>
        <translation>成功</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1603"/>
        <source>Successfully created chan. To let others join your chan, give them the chan name and this Bitmessage address: %1. This address also appears in 'Your Identities'.</source>
        <translation>成功創建頻道。 要讓其他人假如您的頻道，請提供給對方頻道名稱以及這個 Bitmessage 地址: %1. 這個地址也會出現在'您的身份'列表裡。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1612"/>
        <source>Address too new</source>
        <translation>地址太新</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1612"/>
        <source>Although that Bitmessage address might be valid, its version number is too new for us to handle. Perhaps you need to upgrade Bitmessage.</source>
        <translation>雖然 Bitmessage 地址可能是正確的，但是他的版本太新，請更新 Bitmessage。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1616"/>
        <source>Address invalid</source>
        <translation>地址不存在</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1616"/>
        <source>That Bitmessage address is not valid.</source>
        <translation>這個 Bitmessage 地址錯誤</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1624"/>
        <source>Address does not match chan name</source>
        <translation>地址與頻道名稱不符合</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1624"/>
        <source>Although the Bitmessage address you entered was valid, it doesn't match the chan name.</source>
        <translation>雖然您輸入的 Bitmessage 地址是正確的，但是跟頻道名稱並不符合</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1632"/>
        <source>Successfully joined chan. </source>
        <translation>成功加入頻道</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1523"/>
        <source>Connection lost</source>
        <translation>連接已丟失</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1569"/>
        <source>Connected</source>
        <translation>已經連接</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1686"/>
        <source>Message trashed</source>
        <translation>消息已經移入垃圾桶</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1770"/>
        <source>The TTL, or Time-To-Live is the length of time that the network will hold the message.
 The recipient must get it during this time. If your Bitmessage client does not hear an acknowledgement, it
 will resend the message automatically. The longer the Time-To-Live, the
 more work your computer must do to send the message. A Time-To-Live of four or five days is often appropriate.</source>
        <translation>這TTL，或Time-To-Time是保留信息網絡時間的長度.
收件人必須在此期間得到它. 如果您的Bitmessage客戶沒有聽到確認, 它會自動重新發送信息. Time-To-Live的時間越長, 您的電腦必須要做更多工作來發送信息. 四天或五天的 Time-To-Time, 經常是合適的.</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1808"/>
        <source>Message too long</source>
        <translation>信息太長</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1808"/>
        <source>The message that you are trying to send is too long by %1 bytes. (The maximum is 261644 bytes). Please cut it down before sending.</source>
        <translation>你正在嘗試發送的信息已超過％1個位元組太長, (最大為261644個位元組). 發送前請剪下來。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1844"/>
        <source>Error: Your account wasn't registered at an email gateway. Sending registration now as %1, please wait for the registration to be processed before retrying sending.</source>
        <translation>錯誤: 您的帳戶沒有在電子郵件網關註冊。現在發送註冊為％1​​, 註冊正在處理請稍候重試發送.</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2008"/>
        <source>Error: Bitmessage addresses start with BM-   Please check %1</source>
        <translation>錯誤: Bitmessage 地址開頭為 BM-   請檢查 %1</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2011"/>
        <source>Error: The address %1 is not typed or copied correctly. Please check it.</source>
        <translation>錯誤: 地址 %1 輸入錯誤，請重新檢查</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2014"/>
        <source>Error: The address %1 contains invalid characters. Please check it.</source>
        <translation>錯誤: 地址 %1 裡面有錯誤的字元， 請重新檢查。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2017"/>
        <source>Error: The address version in %1 is too high. Either you need to upgrade your Bitmessage software or your acquaintance is being clever.</source>
        <translation>錯誤: 地址 %1 版本太高。 請更新 Bitmessage。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2020"/>
        <source>Error: Some data encoded in the address %1 is too short. There might be something wrong with the software of your acquaintance.</source>
        <translation>錯誤: 在 %1 地址裡面的編碼太短。 您的朋友可能有些軟體上的錯誤。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2023"/>
        <source>Error: Some data encoded in the address %1 is too long. There might be something wrong with the software of your acquaintance.</source>
        <translation>錯誤: 在 %1 地址裡面的編碼太長。 您的朋友可能有些軟體上的錯誤。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2026"/>
        <source>Error: Some data encoded in the address %1 is malformed. There might be something wrong with the software of your acquaintance.</source>
        <translation>錯誤: 在 %1 地址裡面的編碼錯誤。 您的朋友可能有些軟體上的錯誤。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2029"/>
        <source>Error: Something is wrong with the address %1.</source>
        <translation>錯誤: 地址 %1 內有些錯誤。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1946"/>
        <source>Error: You must specify a From address. If you don't have one, go to the 'Your Identities' tab.</source>
        <translation>錯誤： 您必須指出一個表單地址， 如果您沒有，請到“您的身份”標籤頁。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1887"/>
        <source>Address version number</source>
        <translation>地址版本號</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1887"/>
        <source>Concerning the address %1, Bitmessage cannot understand address version numbers of %2. Perhaps upgrade Bitmessage to the latest version.</source>
        <translation>地址 %1 的地址版本號 %2 無法被比特信理解。也許你應該升級你的比特信到最新版本。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1891"/>
        <source>Stream number</source>
        <translation>節點流序號</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1891"/>
        <source>Concerning the address %1, Bitmessage cannot handle stream numbers of %2. Perhaps upgrade Bitmessage to the latest version.</source>
        <translation>地址 %1 的節點流序號 %2 無法被比特信理解。也許你應該升級你的比特信到最新版本。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1896"/>
        <source>Warning: You are currently not connected. Bitmessage will do the work necessary to send the message but it won't send until you connect.</source>
        <translation>警告： 您尚未連接。 比特信將做足夠的功來發送消息，但是消息不會被發出直到您連接。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1938"/>
        <source>Message queued.</source>
        <translation>信息排隊。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1942"/>
        <source>Your 'To' field is empty.</source>
        <translation>“收件人"是空的。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1997"/>
        <source>Right click one or more entries in your address book and select 'Send message to this address'.</source>
        <translation>在您的地址本的一個條目上右擊，之後選擇”發送消息到這個地址“。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2010"/>
        <source>Fetched address from namecoin identity.</source>
        <translation>已經自namecoin接收了地址。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2114"/>
        <source>New Message</source>
        <translation>新消息</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2260"/>
        <source>From </source>
        <translation>從 </translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2506"/>
        <source>Sending email gateway registration request</source>
        <translation>發送電​​子郵件網關註冊請求</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/blacklist.py" line="59"/>
        <source>Address is valid.</source>
        <translation>地址有效。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/blacklist.py" line="93"/>
        <source>The address you entered was invalid. Ignoring it.</source>
        <translation>您輸入的地址是無效的，將被忽略。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2942"/>
        <source>Error: You cannot add the same address to your address book twice. Try renaming the existing one if you want.</source>
        <translation>錯誤：您無法將一個地址添加到您的地址本兩次，請嘗試重命名已經存在的那個。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="3190"/>
        <source>Error: You cannot add the same address to your subscriptions twice. Perhaps rename the existing one if you want.</source>
        <translation>錯誤: 您不能在同一地址添加到您的訂閲兩次. 也許您可重命名現有之一.</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2266"/>
        <source>Restart</source>
        <translation>重啟</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2252"/>
        <source>You must restart Bitmessage for the port number change to take effect.</source>
        <translation>您必須重啟以便使比特信對於使用的連接埠的改變生效。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2266"/>
        <source>Bitmessage will use your proxy from now on but you may want to manually restart Bitmessage now to close existing connections (if any).</source>
        <translation>比特信將會從現在開始使用代理，但是您可能想手動重啟比特信以便使之前的連接關閉（如果有的話）。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2295"/>
        <source>Number needed</source>
        <translation>需求數字</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2295"/>
        <source>Your maximum download and upload rate must be numbers. Ignoring what you typed.</source>
        <translation>您最大的下載和上傳速率必須是數字. 忽略您鍵入的內容.</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2375"/>
        <source>Will not resend ever</source>
        <translation>不嘗試再次發送</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2375"/>
        <source>Note that the time limit you entered is less than the amount of time Bitmessage waits for the first resend attempt therefore your messages will never be resent.</source>
        <translation>請注意，您所輸入的時間限制小於比特信的最小重試時間，因此您將永遠不會重發消息。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2479"/>
        <source>Sending email gateway unregistration request</source>
        <translation>發送電​​子郵件網關註銷請求</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2483"/>
        <source>Sending email gateway status request</source>
        <translation>發送電​​子郵件網關狀態請求</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2583"/>
        <source>Passphrase mismatch</source>
        <translation>密鑰不匹配</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2583"/>
        <source>The passphrase you entered twice doesn't match. Try again.</source>
        <translation>您兩次輸入的密碼並不匹配，請再試一次。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2586"/>
        <source>Choose a passphrase</source>
        <translation>選擇一個密鑰</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2586"/>
        <source>You really do need a passphrase.</source>
        <translation>您真的需要一個密碼。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2883"/>
        <source>Address is gone</source>
        <translation>已經失去了地址</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2883"/>
        <source>Bitmessage cannot find your address %1. Perhaps you removed it?</source>
        <translation>比特信無法找到你的地址 %1。 也許你已經把它刪掉了？</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2886"/>
        <source>Address disabled</source>
        <translation>地址已經禁用</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2886"/>
        <source>Error: The address from which you are trying to send is disabled. You'll have to enable it on the 'Your Identities' tab before using it.</source>
        <translation>錯誤： 您想以一個您已經禁用的地址發出消息。在使用之前您需要在“您的身份”處再次啟用。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2939"/>
        <source>Entry added to the Address Book. Edit the label to your liking.</source>
        <translation>條目已經添加到地址本。您可以去修改您的標籤。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2964"/>
        <source>Entry added to the blacklist. Edit the label to your liking.</source>
        <translation>條目添加到黑名單. 根據自己的喜好編輯標籤.</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2967"/>
        <source>Error: You cannot add the same address to your blacklist twice. Try renaming the existing one if you want.</source>
        <translation>錯誤: 您不能在同一地址添加到您的黑名單兩次.  也許您可重命名現有之一.</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="3095"/>
        <source>Moved items to trash.</source>
        <translation>已經移動項目到垃圾桶。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="3035"/>
        <source>Undeleted item.</source>
        <translation>未刪除的項目。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="3063"/>
        <source>Save As...</source>
        <translation>另存為...</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="3072"/>
        <source>Write error.</source>
        <translation>寫入失敗。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="3176"/>
        <source>No addresses selected.</source>
        <translation>沒有選擇地址。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="3223"/>
        <source>If you delete the subscription, messages that you already received will become inaccessible. Maybe you can consider disabling the subscription instead. Disabled subscriptions will not receive new messages, but you can still view messages you already received.

Are you sure you want to delete the subscription?</source>
        <translation>如果刪除訂閲, 您已經收到的信息將無法訪問. 也許你可以考慮禁用訂閲.禁用訂閲將不會收到新信息, 但您仍然可以看到你已經收到的信息.

你確定要刪除訂閲?</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="3453"/>
        <source>If you delete the channel, messages that you already received will become inaccessible. Maybe you can consider disabling the channel instead. Disabled channels will not receive new messages, but you can still view messages you already received.

Are you sure you want to delete the channel?</source>
        <translation>如果您刪除的頻道, 你已經收到的信息將無法訪問. 也許你可以考慮禁用頻道. 禁用頻道將不會收到新信息, 但你仍然可以看到你已經收到的信息.

你確定要刪除頻道？</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="3568"/>
        <source>Do you really want to remove this avatar?</source>
        <translation>您真的想移除這個頭像麼？</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="3576"/>
        <source>You have already set an avatar for this address. Do you really want to overwrite it?</source>
        <translation>您已經為這個地址設置了頭像了。您真的想移除麼？</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="4022"/>
        <source>Start-on-login not yet supported on your OS.</source>
        <translation>登錄時啟動尚未支持您在使用的操作系統。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="4015"/>
        <source>Minimize-to-tray not yet supported on your OS.</source>
        <translation>最小化到托盤尚未支持您的操作系統。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="4018"/>
        <source>Tray notifications not yet supported on your OS.</source>
        <translation>托盤提醒尚未支持您所使用的操作系統。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="4189"/>
        <source>Testing...</source>
        <translation>正在測試...</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="4229"/>
        <source>This is a chan address. You cannot use it as a pseudo-mailing list.</source>
        <translation>這是一個頻道地址，您無法把它作為偽郵件列表。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="4289"/>
        <source>The address should start with ''BM-''</source>
        <translation>地址應該以"BM-"開始</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="4292"/>
        <source>The address is not typed or copied correctly (the checksum failed).</source>
        <translation>地址沒有被正確的鍵入或複製（校驗碼校驗失敗）。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="4295"/>
        <source>The version number of this address is higher than this software can support. Please upgrade Bitmessage.</source>
        <translation>這個地址的版本號大於此軟件的最大支持。 請升級比特信。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="4298"/>
        <source>The address contains invalid characters.</source>
        <translation>這個地址中包含無效字元。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="4301"/>
        <source>Some data encoded in the address is too short.</source>
        <translation>在這個地址中編碼的部分信息過少。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="4304"/>
        <source>Some data encoded in the address is too long.</source>
        <translation>在這個地址中編碼的部分信息過長。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="4307"/>
        <source>Some data encoded in the address is malformed.</source>
        <translation>在地址編碼的某些數據格式不正確.</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="4281"/>
        <source>Enter an address above.</source>
        <translation>請在上方鍵入地址。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="4313"/>
        <source>Address is an old type. We cannot display its past broadcasts.</source>
        <translation>地址沒有近期的廣播。我們無法顯示之間的廣播。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="4322"/>
        <source>There are no recent broadcasts from this address to display.</source>
        <translation>沒有可以顯示的近期廣播。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="4356"/>
        <source>You are using TCP port %1. (This can be changed in the settings).</source>
        <translation>您正在使用TCP連接埠 %1 。（可以在設置中修改）。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="645"/>
        <source>Bitmessage</source>
        <translation>比特信</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="646"/>
        <source>Identities</source>
        <translation>身份標識</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="647"/>
        <source>New Identity</source>
        <translation>新身份標識</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="709"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="710"/>
        <source>All</source>
        <translation>全部</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="717"/>
        <source>To</source>
        <translation>至</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="719"/>
        <source>From</source>
        <translation>來自</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="721"/>
        <source>Subject</source>
        <translation>標題</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="714"/>
        <source>Message</source>
        <translation>消息</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="723"/>
        <source>Received</source>
        <translation>接收時間</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="663"/>
        <source>Messages</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="666"/>
        <source>Address book</source>
        <translation>地址簿</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="668"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="669"/>
        <source>Add Contact</source>
        <translation>增加聯繫人</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="670"/>
        <source>Fetch Namecoin ID</source>
        <translation>接收Namecoin ID</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="677"/>
        <source>Subject:</source>
        <translation>標題：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="676"/>
        <source>From:</source>
        <translation>來自：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="673"/>
        <source>To:</source>
        <translation>至：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="675"/>
        <source>Send ordinary Message</source>
        <translation>發送普通信息</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="679"/>
        <source>Send Message to your Subscribers</source>
        <translation>發送信息給您的訂戶</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="680"/>
        <source>TTL:</source>
        <translation>TTL:</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="706"/>
        <source>Subscriptions</source>
        <translation>訂閲</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="690"/>
        <source>Add new Subscription</source>
        <translation>添加新的訂閲</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="724"/>
        <source>Chans</source>
        <translation>頻道</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="708"/>
        <source>Add Chan</source>
        <translation>添加 Chans</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="729"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="740"/>
        <source>Settings</source>
        <translation>設置</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="736"/>
        <source>Help</source>
        <translation>幫助</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="732"/>
        <source>Import keys</source>
        <translation>導入密鑰</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="733"/>
        <source>Manage keys</source>
        <translation>管理密鑰</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="735"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="737"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="738"/>
        <source>Contact support</source>
        <translation>聯繫支持</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="739"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="741"/>
        <source>Regenerate deterministic addresses</source>
        <translation>重新生成靜態地址</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="742"/>
        <source>Delete all trashed messages</source>
        <translation>徹底刪除全部垃圾桶中的消息</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="743"/>
        <source>Join / Create chan</source>
        <translation>加入或創建一個頻道</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/foldertree.py" line="172"/>
        <source>All accounts</source>
        <translation>所有帳戶</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/messageview.py" line="47"/>
        <source>Zoom level %1%</source>
        <translation>縮放級別％1％</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/blacklist.py" line="90"/>
        <source>Error: You cannot add the same address to your list twice. Perhaps rename the existing one if you want.</source>
        <translation>錯誤: 您不能在同一地址添加到列表中兩次. 也許您可重命名現有之一.</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/blacklist.py" line="111"/>
        <source>Add new entry</source>
        <translation>添加新條目</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="4326"/>
        <source>Display the %1 recent broadcast(s) from this address.</source>
        <translation>顯示從這個地址％1的最近廣播</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1695"/>
        <source>New version of PyBitmessage is available: %1. Download it from https://github.com/Bitmessage/PyBitmessage/releases/latest</source>
        <translation>PyBitmessage的新版本可用: %1. 從https://github.com/Bitmessage/PyBitmessage/releases/latest下載</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2679"/>
        <source>Waiting for PoW to finish... %1%</source>
        <translation>等待PoW完成...%1%</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2683"/>
        <source>Shutting down Pybitmessage... %1%</source>
        <translation>關閉Pybitmessage ...%1%</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2694"/>
        <source>Waiting for objects to be sent... %1%</source>
        <translation>等待要發送對象...%1%</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2704"/>
        <source>Saving settings... %1%</source>
        <translation>保存設置...%1%</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2713"/>
        <source>Shutting down core... %1%</source>
        <translation>關閉核心...%1%</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2716"/>
        <source>Stopping notifications... %1%</source>
        <translation>停止通知...%1%</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2719"/>
        <source>Shutdown imminent... %1%</source>
        <translation>關閉即將來臨...%1%</translation>
    </message>
    <message numerus="yes">
        <location filename="../bitmessageqt/bitmessageui.py" line="686"/>
        <source>%n hour(s)</source>
        <translation><numerusform>%n 小時</numerusform></translation>
    </message>
    <message numerus="yes">
        <location filename="../bitmessageqt/__init__.py" line="835"/>
        <source>%n day(s)</source>
        <translation><numerusform>%n 天</numerusform></translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2651"/>
        <source>Shutting down PyBitmessage... %1%</source>
        <translation>關閉PyBitmessage...%1%</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1104"/>
        <source>Sent</source>
        <translation>發送</translation>
    </message>
    <message>
        <location filename="../class_addressGenerator.py" line="91"/>
        <source>Generating one new address</source>
        <translation>生成一個新的地址</translation>
    </message>
    <message>
        <location filename="../class_addressGenerator.py" line="153"/>
        <source>Done generating address. Doing work necessary to broadcast it...</source>
        <translation>完成生成地址. 做必要的工作, 以播放它...</translation>
    </message>
    <message>
        <location filename="../class_addressGenerator.py" line="170"/>
        <source>Generating %1 new addresses.</source>
        <translation>生成%1個新地址.</translation>
    </message>
    <message>
        <location filename="../class_addressGenerator.py" line="247"/>
        <source>%1 is already in 'Your Identities'. Not adding it again.</source>
        <translation>%1已經在'您的身份'. 不必重新添加.</translation>
    </message>
    <message>
        <location filename="../class_addressGenerator.py" line="283"/>
        <source>Done generating address</source>
        <translation>完成生成地址</translation>
    </message>
    <message>
        <location filename="../class_outgoingSynSender.py" line="228"/>
        <source>SOCKS5 Authentication problem: %1</source>
        <translation>SOCKS5 驗證錯誤: %1</translation>
    </message>
    <message>
        <location filename="../class_sqlThread.py" line="584"/>
        <source>Disk full</source>
        <translation>磁碟已滿</translation>
    </message>
    <message>
        <location filename="../class_sqlThread.py" line="584"/>
        <source>Alert: Your disk or data storage volume is full. Bitmessage will now exit.</source>
        <translation>警告: 您的磁碟或數據存儲量已滿. 比特信將立即退出.</translation>
    </message>
    <message>
        <location filename="../class_singleWorker.py" line="737"/>
        <source>Error! Could not find sender address (your address) in the keys.dat file.</source>
        <translation>錯誤! 找不到在keys.dat 件發件人的地址 ( 您的地址).</translation>
    </message>
    <message>
        <location filename="../class_singleWorker.py" line="485"/>
        <source>Doing work necessary to send broadcast...</source>
        <translation>做必要的工作, 以發送廣播...</translation>
    </message>
    <message>
        <location filename="../class_singleWorker.py" line="508"/>
        <source>Broadcast sent on %1</source>
        <translation>廣播發送%1</translation>
    </message>
    <message>
        <location filename="../class_singleWorker.py" line="577"/>
        <source>Encryption key was requested earlier.</source>
        <translation>加密密鑰已請求.</translation>
    </message>
    <message>
        <location filename="../class_singleWorker.py" line="614"/>
        <source>Sending a request for the recipient's encryption key.</source>
        <translation>發送收件人的加密密鑰的請求.</translation>
    </message>
    <message>
        <location filename="../class_singleWorker.py" line="629"/>
        <source>Looking up the receiver's public key</source>
        <translation>展望接收方的公鑰</translation>
    </message>
    <message>
        <location filename="../class_singleWorker.py" line="663"/>
        <source>Problem: Destination is a mobile device who requests that the destination be included in the message but this is disallowed in your settings.  %1</source>
        <translation>問題: 目標是移動電話設備所請求的目的地包括在消息中, 但是這是在你的設置禁止. %1</translation>
    </message>
    <message>
        <location filename="../class_singleWorker.py" line="677"/>
        <source>Doing work necessary to send message.
There is no required difficulty for version 2 addresses like this.</source>
        <translation>做必要的工作, 以發送信息.
這樣第2版的地址沒有難度.</translation>
    </message>
    <message>
        <location filename="../class_singleWorker.py" line="691"/>
        <source>Doing work necessary to send message.
Receiver's required difficulty: %1 and %2</source>
        <translation>做必要的工作, 以發送短信.
接收者的要求難度: %1與%2</translation>
    </message>
    <message>
        <location filename="../class_singleWorker.py" line="700"/>
        <source>Problem: The work demanded by the recipient (%1 and %2) is more difficult than you are willing to do. %3</source>
        <translation>問題: 由接收者(%1%2)要求的工作量比您願意做的工作量來得更困難. %3</translation>
    </message>
    <message>
        <location filename="../class_singleWorker.py" line="712"/>
        <source>Problem: You are trying to send a message to yourself or a chan but your encryption key could not be found in the keys.dat file. Could not encrypt message. %1</source>
        <translation>問題: 您正在嘗試將信息發送給自己或頻道, 但您的加密密鑰無法在keys.dat文件中找到. 無法加密信息. %1</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1006"/>
        <source>Doing work necessary to send message.</source>
        <translation>做必要的工作, 以發送信息.</translation>
    </message>
    <message>
        <location filename="../class_singleWorker.py" line="835"/>
        <source>Message sent. Waiting for acknowledgement. Sent on %1</source>
        <translation>信息發送. 等待確認. 已發送%1</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="994"/>
        <source>Doing work necessary to request encryption key.</source>
        <translation>做必要的工作以要求加密密鑰.</translation>
    </message>
    <message>
        <location filename="../class_singleWorker.py" line="951"/>
        <source>Broadcasting the public key request. This program will auto-retry if they are offline.</source>
        <translation>廣播公鑰請求. 這個程序將自動重試, 如果他們處於離線狀態.</translation>
    </message>
    <message>
        <location filename="../class_singleWorker.py" line="953"/>
        <source>Sending public key request. Waiting for reply. Requested at %1</source>
        <translation>發送公鑰的請求. 等待回覆. 請求在%1</translation>
    </message>
    <message>
        <location filename="../upnp.py" line="235"/>
        <source>UPnP port mapping established on port %1</source>
        <translation>UPnP連接埠映射建立在連接埠%1</translation>
    </message>
    <message>
        <location filename="../upnp.py" line="264"/>
        <source>UPnP port mapping removed</source>
        <translation>UPnP連接埠映射被刪除</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="261"/>
        <source>Mark all messages as read</source>
        <translation>標記全部信息為已讀</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2525"/>
        <source>Are you sure you would like to mark all messages read?</source>
        <translation>確定將所有信息標記為已讀嗎？</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1015"/>
        <source>Doing work necessary to send broadcast.</source>
        <translation>持續進行必要的工作，以發送廣播。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2618"/>
        <source>Proof of work pending</source>
        <translation>待傳輸內容的校驗</translation>
    </message>
    <message numerus="yes">
        <location filename="../bitmessageqt/__init__.py" line="2618"/>
        <source>%n object(s) pending proof of work</source>
        <translation><numerusform>%n 待傳輸內容校驗任務</numerusform></translation>
    </message>
    <message numerus="yes">
        <location filename="../bitmessageqt/__init__.py" line="2618"/>
        <source>%n object(s) waiting to be distributed</source>
        <translation><numerusform>%n 任務等待分配</numerusform></translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2618"/>
        <source>Wait until these tasks finish?</source>
        <translation>等待所有任務執行完？</translation>
    </message>
    <message>
        <location filename="../class_outgoingSynSender.py" line="211"/>
        <source>Problem communicating with proxy: %1. Please check your network settings.</source>
        <translation>與代理通信故障率：%1。請檢查你的網絡連接。</translation>
    </message>
    <message>
        <location filename="../class_outgoingSynSender.py" line="240"/>
        <source>SOCKS5 Authentication problem: %1. Please check your SOCKS5 settings.</source>
        <translation>SOCK5認證錯誤：%1。請檢查你的SOCK5設置。</translation>
    </message>
    <message>
        <location filename="../class_receiveDataThread.py" line="171"/>
        <source>The time on your computer, %1, may be wrong. Please verify your settings.</source>
        <translation>你電腦上時間有誤：%1。請檢查你的設置。</translation>
    </message>
    <message>
        <location filename="../namecoin.py" line="101"/>
        <source>The name %1 was not found.</source>
        <translation>名字%1未找到。</translation>
    </message>
    <message>
        <location filename="../namecoin.py" line="110"/>
        <source>The namecoin query failed (%1)</source>
        <translation>域名幣查詢失敗(%1)</translation>
    </message>
    <message>
        <location filename="../namecoin.py" line="113"/>
        <source>The namecoin query failed.</source>
        <translation>域名幣查詢失敗。</translation>
    </message>
    <message>
        <location filename="../namecoin.py" line="119"/>
        <source>The name %1 has no valid JSON data.</source>
        <translation>名字%1沒有有效地JSON數據。</translation>
    </message>
    <message>
        <location filename="../namecoin.py" line="127"/>
        <source>The name %1 has no associated Bitmessage address.</source>
        <translation> 名字%1沒有關聯比特信地址。</translation>
    </message>
    <message>
        <location filename="../namecoin.py" line="147"/>
        <source>Success!  Namecoind version %1 running.</source>
        <translation>成功！域名幣系統%1運行中。</translation>
    </message>
    <message>
        <location filename="../namecoin.py" line="153"/>
        <source>Success!  NMControll is up and running.</source>
        <translation>成功！域名幣控制上線運行！</translation>
    </message>
    <message>
        <location filename="../namecoin.py" line="156"/>
        <source>Couldn't understand NMControl.</source>
        <translation>不能理解 NMControl。</translation>
    </message>
    <message>
        <location filename="../proofofwork.py" line="120"/>
        <source>Your GPU(s) did not calculate correctly, disabling OpenCL. Please report to the developers.</source>
        <translation>你的GPU(s)不能夠正確計算，關閉OpenGL。請報告給開發者。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="3616"/>
        <source>Set notification sound...</source>
        <translation>設置通知提示音...</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="648"/>
        <source>
        Welcome to easy and secure Bitmessage
            * send messages to other people
            * send broadcast messages like twitter or
            * discuss in chan(nel)s with other people
        </source>
        <translation>
歡迎使用簡便安全的比特信
*發送信息給其他人
*像推特那樣發送廣播信息
*在 頻道 裡和其他人討論</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="826"/>
        <source>not recommended for chans</source>
        <translation>頻道內不建議的內容</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1168"/>
        <source>Quiet Mode</source>
        <translation>靜默模式</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1529"/>
        <source>Problems connecting? Try enabling UPnP in the Network Settings</source>
        <translation>連接問題？請嘗試在網絡設置裡打開UPnP</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1831"/>
        <source>You are trying to send an email instead of a bitmessage. This requires registering with a gateway. Attempt to register?</source>
        <translation>您將要嘗試經由 Bitmessage 發送一封電子郵件。該操作需要在一個網關上註冊。嘗試註冊？</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1857"/>
        <source>Error: Bitmessage addresses start with BM-   Please check the recipient address %1</source>
        <translation>錯誤：Bitmessage地址是以BM-開頭的，請檢查收信地址%1.</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1860"/>
        <source>Error: The recipient address %1 is not typed or copied correctly. Please check it.</source>
        <translation>錯誤：收信地址%1未填寫或複製錯誤。請檢查。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1863"/>
        <source>Error: The recipient address %1 contains invalid characters. Please check it.</source>
        <translation>錯誤：收信地址%1還有非法字元。請檢查。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1866"/>
        <source>Error: The version of the recipient address %1 is too high. Either you need to upgrade your Bitmessage software or your acquaintance is being clever.</source>
        <translation>錯誤：收信地址%1版本太高。要麼你需要更新你的軟件，要麼對方需要降級 。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1869"/>
        <source>Error: Some data encoded in the recipient address %1 is too short. There might be something wrong with the software of your acquaintance.</source>
        <translation>錯誤：收信地址%1編碼數據太短。可能對方使用的軟件有問題。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1872"/>
        <source>Error: Some data encoded in the recipient address %1 is too long. There might be something wrong with the software of your acquaintance.</source>
        <translation>錯誤：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1875"/>
        <source>Error: Some data encoded in the recipient address %1 is malformed. There might be something wrong with the software of your acquaintance.</source>
        <translation>錯誤：收信地址%1編碼數據太長。可能對方使用的軟件有問題。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="1878"/>
        <source>Error: Something is wrong with the recipient address %1.</source>
        <translation>錯誤：收信地址%1有問題。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2005"/>
        <source>Error: %1</source>
        <translation>錯誤：%1</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2114"/>
        <source>From %1</source>
        <translation>來自 %1</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2629"/>
        <source>Synchronisation pending</source>
        <translation>待同步</translation>
    </message>
    <message numerus="yes">
        <location filename="../bitmessageqt/__init__.py" line="2629"/>
        <source>Bitmessage hasn't synchronised with the network, %n object(s) to be downloaded. If you quit now, it may cause delivery delays. Wait until the synchronisation finishes?</source>
        <translation><numerusform>Bitmessage還沒有與網絡同步，%n 件任務需要下載。如果你現在退出軟件，可能會造成傳輸延時。是否等同步完成？</numerusform></translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2640"/>
        <source>Not connected</source>
        <translation>未連接成功。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2640"/>
        <source>Bitmessage isn't connected to the network. If you quit now, it may cause delivery delays. Wait until connected and the synchronisation finishes?</source>
        <translation>Bitmessage未連接到網絡。如果現在退出軟件，可能會造成傳輸延時。是否等待同步完成？</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2655"/>
        <source>Waiting for network connection...</source>
        <translation>等待網絡連接……</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="2663"/>
        <source>Waiting for finishing synchronisation...</source>
        <translation>等待同步完成……</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/__init__.py" line="3634"/>
        <source>You have already set a notification sound for this address book entry. Do you really want to overwrite it?</source>
        <translation>您為了這個地址設置過通知音效了。 您想要覆蓋設置嗎？</translation>
    </message>
</context>
<context>
    <name>MessageView</name>
    <message>
        <location filename="../bitmessageqt/messageview.py" line="67"/>
        <source>Follow external link</source>
        <translation>查看外部連結</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/messageview.py" line="67"/>
        <source>The link "%1" will open in a browser. It may be a security risk, it could de-anonymise you or download malicious data. Are you sure?</source>
        <translation>此連結“%1”將在瀏覽器中打開。可能會有安全風險，可能會暴露你或下載惡意數據。確定嗎？</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/messageview.py" line="112"/>
        <source>HTML detected, click here to display</source>
        <translation>檢測到HTML，單擊此處來顯示內容。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/messageview.py" line="121"/>
        <source>Click here to disable HTML</source>
        <translation>單擊此處以禁止HTML。</translation>
    </message>
</context>
<context>
    <name>MsgDecode</name>
    <message>
        <location filename="../helper_msgcoding.py" line="72"/>
        <source>The message has an unknown encoding.
Perhaps you should upgrade Bitmessage.</source>
        <translation>這些消息使用了未知編碼方式。
你可能需要更新Bitmessage軟件。</translation>
    </message>
    <message>
        <location filename="../helper_msgcoding.py" line="73"/>
        <source>Unknown encoding</source>
        <translation>未知編碼</translation>
    </message>
</context>
<context>
    <name>NewAddressDialog</name>
    <message>
        <location filename="../bitmessageqt/newaddressdialog.py" line="173"/>
        <source>Create new Address</source>
        <translation>創建新地址</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newaddressdialog.py" line="174"/>
        <source>Here you may generate as many addresses as you like. Indeed, creating and abandoning addresses is encouraged. You may generate addresses by using either random numbers or by using a passphrase. If you use a passphrase, the address is called a "deterministic" address.
The 'Random Number' option is selected by default but deterministic addresses have several pros and cons:</source>
        <translation>在這裡，您想創建多少地址就創建多少。誠然，創建和丟棄地址受到鼓勵。你既可以使用隨機數來創建地址，也可以使用密鑰。如果您使用密鑰的話，生成的地址叫“靜態地址”。隨機數選項默認為選擇，不過相比而言靜態地址既有缺點也有優點：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newaddressdialog.py" line="176"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Pros:&lt;br/&gt;&lt;/span&gt;You can recreate your addresses on any computer from memory. &lt;br/&gt;You need-not worry about backing up your keys.dat file as long as you can remember your passphrase. &lt;br/&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Cons:&lt;br/&gt;&lt;/span&gt;You must remember (or write down) your passphrase if you expect to be able to recreate your keys if they are lost. &lt;br/&gt;You must remember the address version number and the stream number along with your passphrase. &lt;br/&gt;If you choose a weak passphrase and someone on the Internet can brute-force it, they can read your messages and send messages as you.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;优点:&lt;br/&gt;&lt;/span&gt;您可以通過記憶在任何電腦再次得到您的地址. &lt;br/&gt;您不需要注意備份您的 keys.dat 只要能記住您的密鑰。 &lt;br/&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;缺点:&lt;br/&gt;&lt;/span&gt;您若要再次得到您的地址，您必須牢記（或寫下您的密鑰）。 &lt;br/&gt;您必須牢記密鑰的同時也牢記地址版本號和the stream number . &lt;br/&gt;如果您選擇了一個弱的密鑰的話，一些在互聯網我那個的人可能有機會暴力破解, 他們將可以閲讀您的消息並且以您的身份發送消息.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
        </message>
    <message>
        <location filename="../bitmessageqt/newaddressdialog.py" line="177"/>
        <source>Use a random number generator to make an address</source>
        <translation>使用隨機數生成地址</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newaddressdialog.py" line="178"/>
        <source>Use a passphrase to make addresses</source>
        <translation>使用密鑰生成地址</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newaddressdialog.py" line="179"/>
        <source>Spend several minutes of extra computing time to make the address(es) 1 or 2 characters shorter</source>
        <translation>花費數分鐘的計算使地址短1-2個字母</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newaddressdialog.py" line="180"/>
        <source>Make deterministic addresses</source>
        <translation>創建靜態地址</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newaddressdialog.py" line="181"/>
        <source>Address version number: 4</source>
        <translation>地址版本號:4</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newaddressdialog.py" line="182"/>
        <source>In addition to your passphrase, you must remember these numbers:</source>
        <translation>在記住您的密鑰的同時，您還需要記住以下數字：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newaddressdialog.py" line="183"/>
        <source>Passphrase</source>
        <translation>密鑰</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newaddressdialog.py" line="184"/>
        <source>Number of addresses to make based on your passphrase:</source>
        <translation>使用該密鑰生成的地址數:</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newaddressdialog.py" line="185"/>
        <source>Stream number: 1</source>
        <translation>節點流序號：1</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newaddressdialog.py" line="186"/>
        <source>Retype passphrase</source>
        <translation>再次輸入密鑰</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newaddressdialog.py" line="187"/>
        <source>Randomly generate address</source>
        <translation>隨機生成地址</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newaddressdialog.py" line="188"/>
        <source>Label (not shown to anyone except you)</source>
        <translation>標籤(只有您看的到)</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newaddressdialog.py" line="189"/>
        <source>Use the most available stream</source>
        <translation>使用最可用的節點流</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newaddressdialog.py" line="190"/>
        <source> (best if this is the first of many addresses you will create)</source>
        <translation>如果這是您創建的數個地址中的第一個時最佳</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newaddressdialog.py" line="191"/>
        <source>Use the same stream as an existing address</source>
        <translation>使用和如下地址一樣的節點流</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newaddressdialog.py" line="192"/>
        <source>(saves you some bandwidth and processing power)</source>
        <translation>（節省你的頻寬和處理能力）</translation>
    </message>
</context>
<context>
    <name>NewSubscriptionDialog</name>
    <message>
        <location filename="../bitmessageqt/newsubscriptiondialog.py" line="65"/>
        <source>Add new entry</source>
        <translation>添加新條目</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newsubscriptiondialog.py" line="66"/>
        <source>Label</source>
        <translation>標籤</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newsubscriptiondialog.py" line="67"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newsubscriptiondialog.py" line="68"/>
        <source>Enter an address above.</source>
        <translation>輸入上述地址.</translation>
    </message>
</context>
<context>
    <name>SpecialAddressBehaviorDialog</name>
    <message>
        <location filename="../bitmessageqt/specialaddressbehavior.py" line="59"/>
        <source>Special Address Behavior</source>
        <translation>特別的地址行為</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/specialaddressbehavior.py" line="60"/>
        <source>Behave as a normal address</source>
        <translation>作為普通地址</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/specialaddressbehavior.py" line="61"/>
        <source>Behave as a pseudo-mailing-list address</source>
        <translation>作為偽郵件列表地址</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/specialaddressbehavior.py" line="62"/>
        <source>Mail received to a pseudo-mailing-list address will be automatically broadcast to subscribers (and thus will be public).</source>
        <translation>偽郵件列表收到消息時會自動將其公開的廣播給訂閲者。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/specialaddressbehavior.py" line="63"/>
        <source>Name of the pseudo-mailing-list:</source>
        <translation>偽郵件列表名稱：</translation>
    </message>
</context>
<context>
    <name>aboutDialog</name>
    <message>
        <location filename="../bitmessageqt/about.py" line="68"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/about.py" line="69"/>
        <source>PyBitmessage</source>
        <translation>PyBitmessage</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/about.py" line="70"/>
        <source>version ?</source>
        <translation>版本 ?</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/about.py" line="72"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Distributed under the MIT/X11 software license; see &lt;a href=&quot;http://www.opensource.org/licenses/mit-license.php&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.opensource.org/licenses/mit-license.php&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;以 MIT/X11 軟件授權發佈; 詳情參見 &lt;a href=&quot;http://www.opensource.org/licenses/mit-license.php&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.opensource.org/licenses/mit-license.php&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/about.py" line="73"/>
        <source>This is Beta software.</source>
        <translation>本軟件處於Beta階段。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/about.py" line="70"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Copyright Â© 2012-2016 Jonathan Warren&lt;br/&gt;Copyright Â© 2013-2016 The Bitmessage Developers&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;版權所有 Â© 2012-2016 Jonathan Warren&lt;br/&gt;版權所有 Â© 2013-2016 The Bitmessage Developers&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/about.py" line="71"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Copyright &amp;copy; 2012-2016 Jonathan Warren&lt;br/&gt;Copyright &amp;copy; 2013-2016 The Bitmessage Developers&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;版權：2012-2016 Jonathan Warren&lt;br/&gt;版權： 2013-2016 The Bitmessage Developers&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>blacklist</name>
    <message>
        <location filename="../bitmessageqt/blacklist.ui" line="17"/>
        <source>Use a Blacklist (Allow all incoming messages except those on the Blacklist)</source>
        <translation>使用黑名單 (允許所有傳入的信息除了那些在黑名單)</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/blacklist.ui" line="27"/>
        <source>Use a Whitelist (Block all incoming messages except those on the Whitelist)</source>
        <translation>使用白名單 (阻止所有傳入的消息除了那些在白名單)</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/blacklist.ui" line="34"/>
        <source>Add new entry</source>
        <translation>添加新條目</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/blacklist.ui" line="85"/>
        <source>Name or Label</source>
        <translation>名稱或標籤</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/blacklist.ui" line="90"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/blacklist.py" line="150"/>
        <source>Blacklist</source>
        <translation>黑名單</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/blacklist.py" line="152"/>
        <source>Whitelist</source>
        <translation>白名單</translation>
    </message>
</context>
<context>
    <name>connectDialog</name>
    <message>
        <location filename="../bitmessageqt/connect.py" line="56"/>
        <source>Bitmessage</source>
        <translation>比特信</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/connect.py" line="57"/>
        <source>Bitmessage won't connect to anyone until you let it. </source>
        <translation>除非您允許，比特信不會連接到任何人。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/connect.py" line="58"/>
        <source>Connect now</source>
        <translation>現在連接</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/connect.py" line="59"/>
        <source>Let me configure special network settings first</source>
        <translation>請先讓我進行特別的網絡設置</translation>
    </message>
</context>
<context>
    <name>helpDialog</name>
    <message>
        <location filename="../bitmessageqt/help.py" line="45"/>
        <source>Help</source>
        <translation>幫助</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/help.py" line="46"/>
        <source>&lt;a href=&quot;https://bitmessage.org/wiki/PyBitmessage_Help&quot;&gt;https://bitmessage.org/wiki/PyBitmessage_Help&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;https://bitmessage.org/wiki/PyBitmessage_Help&quot;&gt;https://bitmessage.org/wiki/PyBitmessage_Help&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/help.py" line="47"/>
        <source>As Bitmessage is a collaborative project, help can be found online in the Bitmessage Wiki:</source>
        <translation>鑒於比特信是一個共同完成的項目，您可以在比特信的Wiki上瞭解如何幫助比特信：</translation>
    </message>
</context>
<context>
    <name>iconGlossaryDialog</name>
    <message>
        <location filename="../bitmessageqt/iconglossary.py" line="82"/>
        <source>Icon Glossary</source>
        <translation>圖標含義</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/iconglossary.py" line="83"/>
        <source>You have no connections with other peers. </source>
        <translation>您沒有和其他節點的連接.</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/iconglossary.py" line="84"/>
        <source>You have made at least one connection to a peer using an outgoing connection but you have not yet received any incoming connections. Your firewall or home router probably isn't configured to forward incoming TCP connections to your computer. Bitmessage will work just fine but it would help the Bitmessage network if you allowed for incoming connections and will help you be a better-connected node.</source>
        <translation>你有至少一個到其他節點的出站連接，但是尚未收到入站連接。您的防火牆或路由器可能尚未設置轉發入站TCP連接到您的電腦。比特信將正常運行，不過如果您允許入站連接的話將幫助比特信網絡並成為一個通信狀態更好的節點。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/iconglossary.py" line="85"/>
        <source>You are using TCP port ?. (This can be changed in the settings).</source>
        <translation>您正在使用TCP連接埠 ? 。（可以在設置中更改）.</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/iconglossary.py" line="86"/>
        <source>You do have connections with other peers and your firewall is correctly configured.</source>
        <translation>您有和其他節點的連接且您的防火牆已經正確配置。</translation>
    </message>
</context>
<context>
    <name>networkstatus</name>
    <message>
        <location filename="../bitmessageqt/networkstatus.ui" line="39"/>
        <source>Total connections:</source>
        <translation>總連接:</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.ui" line="185"/>
        <source>Since startup:</source>
        <translation>自啟動:</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.ui" line="201"/>
        <source>Processed 0 person-to-person messages.</source>
        <translation>處理0人對人的信息.</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.ui" line="230"/>
        <source>Processed 0 public keys.</source>
        <translation>處理0公鑰。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.ui" line="217"/>
        <source>Processed 0 broadcasts.</source>
        <translation>處理0廣播.</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.ui" line="282"/>
        <source>Inventory lookups per second: 0</source>
        <translation>每秒庫存查詢: 0</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.ui" line="243"/>
        <source>Objects to be synced:</source>
        <translation>對象	已同步:</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.ui" line="152"/>
        <source>Stream #</source>
        <translation>數據流 ＃</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.ui" line="116"/>
        <source>Connections</source>
        <translation>連接數</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.py" line="162"/>
        <source>Since startup on %1</source>
        <translation>自從%1啟動</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.py" line="81"/>
        <source>Down: %1/s  Total: %2</source>
        <translation>下: %1/秒 總計: %2</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.py" line="83"/>
        <source>Up: %1/s  Total: %2</source>
        <translation>上: %1/秒 總計: %2</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.py" line="144"/>
        <source>Total Connections: %1</source>
        <translation>總的連接數: %1</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.py" line="154"/>
        <source>Inventory lookups per second: %1</source>
        <translation>每秒庫存查詢: %1</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.ui" line="256"/>
        <source>Up: 0 kB/s</source>
        <translation>上載: 0 kB /秒</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.ui" line="269"/>
        <source>Down: 0 kB/s</source>
        <translation>下載: 0 kB /秒</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/bitmessageui.py" line="728"/>
        <source>Network Status</source>
        <translation>網絡狀態</translation>
    </message>
    <message numerus="yes">
        <location filename="../bitmessageqt/networkstatus.py" line="48"/>
        <source>byte(s)</source>
        <translation><numerusform>位元組</numerusform></translation>
    </message>
    <message numerus="yes">
        <location filename="../bitmessageqt/networkstatus.py" line="59"/>
        <source>Object(s) to be synced: %n</source>
        <translation><numerusform>要同步的對象: %n</numerusform></translation>
    </message>
    <message numerus="yes">
        <location filename="../bitmessageqt/networkstatus.py" line="63"/>
        <source>Processed %n person-to-person message(s).</source>
        <translation><numerusform>處理%n人對人的信息.</numerusform></translation>
    </message>
    <message numerus="yes">
        <location filename="../bitmessageqt/networkstatus.py" line="68"/>
        <source>Processed %n broadcast message(s).</source>
        <translation><numerusform>處理%n廣播信息.</numerusform></translation>
    </message>
    <message numerus="yes">
        <location filename="../bitmessageqt/networkstatus.py" line="73"/>
        <source>Processed %n public key(s).</source>
        <translation><numerusform>處理%n公鑰.</numerusform></translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.ui" line="120"/>
        <source>Peer</source>
        <translation>節點</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.ui" line="123"/>
        <source>IP address or hostname</source>
        <translation>IP地址或主機名</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.ui" line="128"/>
        <source>Rating</source>
        <translation>評分</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.ui" line="131"/>
        <source>PyBitmessage tracks the success rate of connection attempts to individual nodes. The rating ranges from -1 to 1 and affects the likelihood of selecting the node in the future</source>
        <translation>PyBitmessage 跟蹤連接嘗試到各個節點的成功率。評分範圍從 -1 到 1 ，這會影響到未來選擇節點的可能性。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.ui" line="136"/>
        <source>User agent</source>
        <translation>用戶代理</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.ui" line="139"/>
        <source>Peer's self-reported software</source>
        <translation>節點自我報告的軟件</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.ui" line="144"/>
        <source>TLS</source>
        <translation>TLS</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.ui" line="147"/>
        <source>Connection encryption</source>
        <translation>連接加密</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/networkstatus.ui" line="155"/>
        <source>List of streams negotiated between you and the peer</source>
        <translation>您和節點之間已協商的流列表</translation>
    </message>
</context>
<context>
    <name>newChanDialog</name>
    <message>
        <location filename="../bitmessageqt/newchandialog.py" line="97"/>
        <source>Dialog</source>
        <translation>診斷</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newchandialog.py" line="98"/>
        <source>Create a new chan</source>
        <translation>建立新的頻道</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newchandialog.py" line="103"/>
        <source>Join a chan</source>
        <translation>加入一個頻道</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newchandialog.py" line="100"/>
        <source>Create a chan</source>
        <translation>建立一個頻道</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newchandialog.py" line="101"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enter a name for your chan. If you choose a sufficiently complex chan name (like a strong and unique passphrase) and none of your friends share it publicly then the chan will be secure and private. If you and someone else both create a chan with the same chan name then it is currently very likely that they will be the same chan.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;輸入您的頻道名稱. 如果您選擇一個很複雜的頻道名稱 (像是一個極其複雜的字串) 而且如果你的朋友都沒有公開分享它，那麼這個頻道將是安全和私密的. 如果你和其他人都用同一個名字創建了一個頻道，那麼它現在很可能會是同一個頻道.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newchandialog.py" line="105"/>
        <source>Chan name:</source>
        <translation>頻道名稱</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newchandialog.py" line="104"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;A chan exists when a group of people share the same decryption keys. The keys and bitmessage address used by a chan are generated from a human-friendly word or phrase (the chan name). To send a message to everyone in the chan, send a normal person-to-person message to the chan address.&lt;/p&gt;&lt;p&gt;Chans are experimental and completely unmoderatable.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;當一群人共享相同的解密密鑰時，頻道就存在。 頻道使用的金鑰和 bitmessage 地址是從友好的單詞或短語（頻道名稱）生成的。 若要發送一個消息給頻道裡面的每個人，只需要發送一個正常的人對人的消息到頻道地址。&lt;/p&gt;&lt;p&gt;頻道為實驗性並且完全無法模擬的.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newchandialog.py" line="106"/>
        <source>Chan bitmessage address:</source>
        <translation>頻道 bitmessage 地址:</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newchandialog.ui" line="26"/>
        <source>Create or join a chan</source>
        <translation>創建或加入一個頻道</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newchandialog.ui" line="41"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;A chan exists when a group of people share the same decryption keys. The keys and bitmessage address used by a chan are generated from a human-friendly word or phrase (the chan name). To send a message to everyone in the chan, send a message to the chan address.&lt;/p&gt;&lt;p&gt;Chans are experimental and completely unmoderatable.&lt;/p&gt;&lt;p&gt;Enter a name for your chan. If you choose a sufficiently complex chan name (like a strong and unique passphrase) and none of your friends share it publicly, then the chan will be secure and private. However if you and someone else both create a chan with the same chan name, the same chan will be shared.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;當一群人共享同一樣的加密鑰匙時會創建一個頻道。使用一個詞組來命名密鑰和bitmessage地址。發送信息到頻道地址就可以發送消息給每個成員。&lt;/p&gt;&lt;p&gt;頻道功能為實驗性功能，也不穩定。&lt;/p&gt;&lt;p&gt;為你的頻道命名。如果你選擇使用一個十分複雜的名字命令並且你的朋友不會公開它，那這個頻道就是安全和私密的。然而如果你和其他人都創建了一個同樣命名的頻道，那麼相同名字的頻道將會被共享。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newchandialog.ui" line="56"/>
        <source>Chan passphrase/name:</source>
        <translation>頻道 字串/名稱:</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newchandialog.ui" line="63"/>
        <source>Optional, for advanced usage</source>
        <translation>可選，適用於高級應用</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newchandialog.ui" line="76"/>
        <source>Chan address</source>
        <translation>頻道地址</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newchandialog.ui" line="101"/>
        <source>Please input chan name/passphrase:</source>
        <translation>請輸入頻道名字：</translation>
    </message>
</context>
<context>
    <name>newchandialog</name>
    <message>
        <location filename="../bitmessageqt/newchandialog.py" line="38"/>
        <source>Successfully created / joined chan %1</source>
        <translation>成功創建或加入頻道%1</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newchandialog.py" line="42"/>
        <source>Chan creation / joining failed</source>
        <translation>頻道創建或加入失敗</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/newchandialog.py" line="48"/>
        <source>Chan creation / joining cancelled</source>
        <translation>頻道創建或加入已取消</translation>
    </message>
</context>
<context>
    <name>proofofwork</name>
    <message>
        <location filename="../proofofwork.py" line="163"/>
        <source>C PoW module built successfully.</source>
        <translation>C PoW模組編譯成功。</translation>
    </message>
    <message>
        <location filename="../proofofwork.py" line="165"/>
        <source>Failed to build C PoW module. Please build it manually.</source>
        <translation>無法編譯C PoW模組。請手動編譯。</translation>
    </message>
    <message>
        <location filename="../proofofwork.py" line="167"/>
        <source>C PoW module unavailable. Please build it.</source>
        <translation>C PoW模組不可用。請編譯它。</translation>
    </message>
</context>
<context>
    <name>qrcodeDialog</name>
    <message>
        <location filename="../plugins/qrcodeui.py" line="67"/>
        <source>QR-code</source>
        <translation>二維碼</translation>
    </message>
</context>
<context>
    <name>regenerateAddressesDialog</name>
    <message>
        <location filename="../bitmessageqt/regenerateaddresses.py" line="114"/>
        <source>Regenerate Existing Addresses</source>
        <translation>重新生成已經存在的地址</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/regenerateaddresses.py" line="115"/>
        <source>Regenerate existing addresses</source>
        <translation>重新生成已經存在的地址</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/regenerateaddresses.py" line="116"/>
        <source>Passphrase</source>
        <translation>密鑰</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/regenerateaddresses.py" line="117"/>
        <source>Number of addresses to make based on your passphrase:</source>
        <translation>您想要要使用這個密鑰生成的地址數：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/regenerateaddresses.py" line="118"/>
        <source>Address version number:</source>
        <translation>地址版本號：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/regenerateaddresses.py" line="119"/>
        <source>Stream number:</source>
        <translation>節點流序號：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/regenerateaddresses.py" line="120"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/regenerateaddresses.py" line="121"/>
        <source>Spend several minutes of extra computing time to make the address(es) 1 or 2 characters shorter</source>
        <translation>花費數分鐘的計算使地址短1-2個字母</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/regenerateaddresses.py" line="122"/>
        <source>You must check (or not check) this box just like you did (or didn't) when you made your addresses the first time.</source>
        <translation>這個選項需要和您第一次生成的時候相同。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/regenerateaddresses.py" line="123"/>
        <source>If you have previously made deterministic addresses but lost them due to an accident (like hard drive failure), you can regenerate them here. If you used the random number generator to make your addresses then this form will be of no use to you.</source>
        <translation>如果您之前創建了靜態地址，但是因為一些意外失去了它們（比如硬盤壞了），您可以在這裡將他們再次生成。如果您使用隨機數來生成的地址的話，那麼這個表格對您沒有幫助。</translation>
    </message>
</context>
<context>
    <name>settingsDialog</name>
    <message>
        <location filename="../bitmessageqt/settings.py" line="453"/>
        <source>Settings</source>
        <translation>設置</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="454"/>
        <source>Start Bitmessage on user login</source>
        <translation>在用戶登錄時啟動比特信</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="455"/>
        <source>Tray</source>
        <translation>任務欄</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="456"/>
        <source>Start Bitmessage in the tray (don't show main window)</source>
        <translation>啟動比特信到托盤 （不要顯示主窗口）</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="457"/>
        <source>Minimize to tray</source>
        <translation>最小化到托盤</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="458"/>
        <source>Close to tray</source>
        <translation>關閉任務欄</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="460"/>
        <source>Show notification when message received</source>
        <translation>在收到消息時提示</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="461"/>
        <source>Run in Portable Mode</source>
        <translation>以便攜方式運行</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="462"/>
        <source>In Portable Mode, messages and config files are stored in the same directory as the program rather than the normal application-data folder. This makes it convenient to run Bitmessage from a USB thumb drive.</source>
        <translation>在便攜模式下， 消息和配置文件和程序保存在同一個目錄而不是通常的程序數據文件夾。 這使在U盤中允許比特信很方便。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="463"/>
        <source>Willingly include unencrypted destination address when sending to a mobile device</source>
        <translation>願意在發送到手機時使用不加密的目標地址</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="464"/>
        <source>Use Identicons</source>
        <translation>用戶身份</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="465"/>
        <source>Reply below Quote</source>
        <translation>回覆	引述如下</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="466"/>
        <source>Interface Language</source>
        <translation>界面語言</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="467"/>
        <source>System Settings</source>
        <comment>system</comment>
        <translation>系統設置</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="468"/>
        <source>User Interface</source>
        <translation>用戶界面</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="469"/>
        <source>Listening port</source>
        <translation>監聽連接埠</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="470"/>
        <source>Listen for connections on port:</source>
        <translation>監聽連接於連接埠：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="471"/>
        <source>UPnP:</source>
        <translation>UPnP:</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="472"/>
        <source>Bandwidth limit</source>
        <translation>頻寬限制</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="473"/>
        <source>Maximum download rate (kB/s): [0: unlimited]</source>
        <translation>最大下載速率(kB/秒): [0: 無限制]</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="474"/>
        <source>Maximum upload rate (kB/s): [0: unlimited]</source>
        <translation>最大上傳速度 (kB/秒): [0: 無限制]</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="476"/>
        <source>Proxy server / Tor</source>
        <translation>代理服務器 / Tor</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="477"/>
        <source>Type:</source>
        <translation>類型：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="478"/>
        <source>Server hostname:</source>
        <translation>服務器主機名：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="501"/>
        <source>Port:</source>
        <translation>連接埠：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="480"/>
        <source>Authentication</source>
        <translation>認證</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="502"/>
        <source>Username:</source>
        <translation>用戶名：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="482"/>
        <source>Pass:</source>
        <translation>密碼：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="483"/>
        <source>Listen for incoming connections when using proxy</source>
        <translation>在使用代理時仍然監聽入站連接</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="484"/>
        <source>none</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="485"/>
        <source>SOCKS4a</source>
        <translation>SOCKS4a</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="486"/>
        <source>SOCKS5</source>
        <translation>SOCKS5</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="487"/>
        <source>Network Settings</source>
        <translation>網絡設置</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="488"/>
        <source>Total difficulty:</source>
        <translation>總難度：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="489"/>
        <source>The 'Total difficulty' affects the absolute amount of work the sender must complete. Doubling this value doubles the amount of work.</source>
        <translation>“總難度”影響發送者所需要的做工總數。當這個值翻倍時，做工的總數也翻倍。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="490"/>
        <source>Small message difficulty:</source>
        <translation>小消息難度：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="491"/>
        <source>When someone sends you a message, their computer must first complete some work. The difficulty of this work, by default, is 1. You may raise this default for new addresses you create by changing the values here. Any new addresses you create will require senders to meet the higher difficulty. There is one exception: if you add a friend or acquaintance to your address book, Bitmessage will automatically notify them when you next send a message that they need only complete the minimum amount of work: difficulty 1. </source>
        <translation>當一個人向您發送消息的時候， 他們的電腦必須先做工。這個難度的默認值是1,您可以在創建新的地址前提高這個值。任何新創建的地址都會要求更高的做工量。這裡有一個例外，當您將您的朋友添加到地址本的時候，比特信將自動提示他們，當他們下一次向您發送的時候，他們需要的做功量將總是1.</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="492"/>
        <source>The 'Small message difficulty' mostly only affects the difficulty of sending small messages. Doubling this value makes it almost twice as difficult to send a small message but doesn't really affect large messages.</source>
        <translation>“小消息困難度”幾乎僅影響發送消息。當這個值翻倍時，發小消息時做工的總數也翻倍，但是並不影響大的消息。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="493"/>
        <source>Demanded difficulty</source>
        <translation>要求的難度</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="494"/>
        <source>Here you may set the maximum amount of work you are willing to do to send a message to another person. Setting these values to 0 means that any value is acceptable.</source>
        <translation>你可以在這裡設置您所願意接受的發送消息的最大難度。0代表接受任何難度。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="495"/>
        <source>Maximum acceptable total difficulty:</source>
        <translation>最大接受難度：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="496"/>
        <source>Maximum acceptable small message difficulty:</source>
        <translation>最大接受的小消息難度：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="497"/>
        <source>Max acceptable difficulty</source>
        <translation>最大可接受難度</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="473"/>
        <source>Hardware GPU acceleration (OpenCL)</source>
        <translation>硬體 GPU 加速(OpenCL)</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="499"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Bitmessage can utilize a different Bitcoin-based program called Namecoin to make addresses human-friendly. For example, instead of having to tell your friend your long Bitmessage address, you can simply tell him to send a message to &lt;span style=&quot; font-style:italic;&quot;&gt;test. &lt;/span&gt;&lt;/p&gt;&lt;p&gt;(Getting your own Bitmessage address into Namecoin is still rather difficult).&lt;/p&gt;&lt;p&gt;Bitmessage can use either namecoind directly or a running nmcontrol instance.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;比特信可以利用基於比特幣的Namecoin讓地址更加友好。比如除了告訴您的朋友您的長長的比特信地址，您還可以告訴他們發消息給 &lt;span style=&quot; font-style:italic;&quot;&gt;test. &lt;/span&gt;&lt;/p&gt;&lt;p&gt;把您的地址放入Namecoin還是相當的難的.&lt;/p&gt;&lt;p&gt;比特信可以不但直接連接到namecoin守護程序或者連接到運行中的nmcontrol實例.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="500"/>
        <source>Host:</source>
        <translation>主機名：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="503"/>
        <source>Password:</source>
        <translation>密碼：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="504"/>
        <source>Test</source>
        <translation>測試</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="505"/>
        <source>Connect to:</source>
        <translation>連接到：</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="506"/>
        <source>Namecoind</source>
        <translation>Namecoind</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="507"/>
        <source>NMControl</source>
        <translation>NMControl</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="508"/>
        <source>Namecoin integration</source>
        <translation>Namecoin整合</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="509"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;By default, if you send a message to someone and he is offline for more than two days, Bitmessage will send the message again after an additional two days. This will be continued with exponential backoff forever; messages will be resent after 5, 10, 20 days ect. until the receiver acknowledges them. Here you may change that behavior by having Bitmessage give up after a certain number of days or months.&lt;/p&gt;&lt;p&gt;Leave these input fields blank for the default behavior. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;您發給他們的消息默認會在網絡上保存兩天，之後比特信會再重發一次. 重發時間會隨指數上升; 消息會在5, 10, 20... 天后重發並以此類推. 直到收到收件人的回執. 你可以在這裡改變這一行為，讓比特信在嘗試一段時間後放棄.&lt;/p&gt;&lt;p&gt;留空意味著默認行為. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="510"/>
        <source>Give up after</source>
        <translation>在</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="511"/>
        <source>and</source>
        <translation>和</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="512"/>
        <source>days</source>
        <translation>天</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="513"/>
        <source>months.</source>
        <translation>月後放棄。</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="514"/>
        <source>Resends Expire</source>
        <translation>重發超時</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="459"/>
        <source>Hide connection notifications</source>
        <translation>隱藏連接通知</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="475"/>
        <source>Maximum outbound connections: [0: none]</source>
        <translation>最大外部連接：[0: 無]</translation>
    </message>
    <message>
        <location filename="../bitmessageqt/settings.py" line="498"/>
        <source>Hardware GPU acceleration (OpenCL):</source>
        <translation>硬件GPU加速(OpenCL)：</translation>
    </message>
</context>
</TS>
